package fr.afpalangueux.lejusteprix;

import java.util.Scanner;
import java.util.Random;

public class LeJustePrix {
    public static boolean Continu = true; // Indique si le jeu doit continuer
    static String prenom = Demande.prenom(); // Le prénom du joueur


    public static void startMenu() {
        /**
         * Affiche le menu du jeu Le Juste Prix et permet au joueur de choisir
         * différentes options, comme lancer le jeu, voir le Scoreboard, changer de joueur, ou quitter.
         */
        // Génère un message contenant le nom du joueur centré dans un cadre
        String message = "Joueur : " + prenom;
        int longueurMessage = 43; // Longueur totale du message (y compris les bords)
        int espacesDeChaqueCote = (longueurMessage - message.length()) / 2;
        String ligneMessage = "█".repeat(3) + " ".repeat(espacesDeChaqueCote)
                + message + " ".repeat(espacesDeChaqueCote) + "█".repeat(3);

        // Affiche le menu
        Print.print("         ███+>>██ Menu du Jeu ██<<-███");
        Print.print("████████████████████████████████████████████████");
        Print.print(ligneMessage);
        Print.print("█████ * Lancer le jeu :      Appuyer sur P █████");
        Print.print("█████ * Voir le Scoreboard : Appuyer sur S █████");
        Print.print("█████ * Changer de Joueur :  Appuyer sur C █████");
        Print.print("█████ * Arrêter le jeu :     Appuyer sur A █████");
        Print.print("████████████████████████████████████████████████\n");

        Scanner scanner = new Scanner(System.in);
        String menuChoix = scanner.next().toLowerCase();

        if (menuChoix.equals("p")) {
            startGame();
        } else if (menuChoix.equals("s")) {
            // Si l'option "Voir le Scoreboard" est choisie, affiche le Scoreboard
            Scoreboard.lireEtTrierScores();
            // Attend une pression de touche pour revenir au menu principal
            Demande.pressAnyKeyToContinue();
            // Retourne au menu principal
            startMenu();
        } else if (menuChoix.equals("c")) {
            // Si l'option "Changer de Joueur" est choisie, permet au joueur de saisir un nouveau prénom
            prenom = Demande.prenom();
            // Retourne au menu principal
            startMenu();
        } else if (menuChoix.equals("a")) {
            // Si l'option "Arrêter le jeu" est choisie, affiche un message de départ
            Print.print("Au revoir et à bientôt " + prenom + "! ."); // Utilisez le prénom saisi
            // Arrête le jeu
            Continu = false;
        } else {
            // Si un choix non valide est saisi, affiche un message d'erreur et retourne au menu principal
            Print.print("Choix non valide");
            startMenu();
        }
        scanner.close();
    }

    static Random random = new Random();
    static Scanner scanprixjoueur = new Scanner(System.in);


    public static void startGame() {
        /**
         * Lance le jeu du Juste Prix où le joueur doit deviner un prix entre 0 et 1000.
         */
        int score = 0;
        // Génère un prix aléatoire entre 0 et 1000
        int prixjuste = random.nextInt(1001);
        Print.print("Devinez le juste prix entre 0 et 1000 : ");

        while (Continu) {
            // Le joueur saisit un prix
            if (scanprixjoueur.hasNextInt()) {
                int prixjoueur = scanprixjoueur.nextInt();
                if (prixjuste == prixjoueur) {
                    // Si le prix du joueur est égal au prix juste, le joueur a gagné
                    score++;
                    // Affiche un message de félicitations
                    Print.print("\u001B[32mFélicitation " + prenom + "!\u001B[0m");
                    Print.print("\u001B[32mVous Avez Trouvé Le Juste Prix !\u001B[0m");
                    Print.print("\u001B[32mVotre Score est de " + score + "!\u001B[0m\n");
                    // Ajoute le score au Scoreboard
                    Scoreboard.ajouterScore(prenom, score);
                    // Attend une pression de touche pour revenir au menu principal
                    Demande.pressAnyKeyToContinue();
                    // Retourne au menu principal
                    startMenu();
                } else if (prixjoueur > prixjuste) {
                    // Si le prix du joueur est trop élevé, affiche un message
                    Print.print("<<< C'est \u001B[34mmoins\u001B[0m que " + prixjoueur + " <<< !\n");
                    score++;
                    scanprixjoueur.nextLine();
                } else if (prixjoueur < prixjuste) {
                    // Si le prix du joueur est trop bas, affiche un message
                    Print.print(">>> C'est \u001B[31mplus\u001B[0m que " + prixjoueur + " >>> !\n");
                    score++;
                    scanprixjoueur.nextLine();
                }
            } else {
                // Si le joueur saisit quelque chose qui n'est pas un nombre, affiche un message d'erreur
                Print.print("Veuillez Entrez Un Nombre entre 1 et 1000.");
                scanprixjoueur.nextLine(); // Pour vider la ligne incorrecte
            }
        }
        // Ferme le scanner de saisie des prix
        scanprixjoueur.close();
    }
}