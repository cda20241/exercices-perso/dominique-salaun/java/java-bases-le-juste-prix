package fr.afpalangueux.lejusteprix;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Scoreboard {
    static String fichierScoreboard = "src/main/java/fr/afpalangueux/scoreboard.txt";

    /**
     * Vérifie l'existence du fichier de scoreboard et le crée s'il n'existe pas.
     */
    public static void verif() {
        File fichier = new File(fichierScoreboard);
        if (!fichier.exists()) {
            Print.print("Création du fichier scoreboard.txt ...");
            try {
                fichier.createNewFile();
                Print.print("Le fichier scoreboard.txt a été créé avec succès.");
            } catch (IOException e) {
                Print.print("Une erreur est survenue lors de la création du fichier scoreboard.txt.");
                e.printStackTrace();
            }
        } else {
            Print.print("Le fichier scoreboard.txt existe déjà.");
        }
    }

    /**
     * Ajoute un score avec le nom du joueur au fichier de scoreboard.
     *
     * @param nomJoueur Le nom du joueur.
     * @param score     Le score du joueur.
     */
    public static void ajouterScore(String nomJoueur, int score) {
        try (FileWriter writer = new FileWriter(fichierScoreboard, true)) {
            writer.write(nomJoueur + ": " + score + "\n");
            Print.print("Score ajouté au scoreboard : " + nomJoueur + ": " + score + "\n");
        } catch (IOException e) {
            Print.print("Une erreur est survenue lors de l'ajout du score au scoreboard.");
            e.printStackTrace();
        }
    }

    /**
     * Lit et affiche tous les scores du scoreboard.
     */
    public static void lireScores() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fichierScoreboard))) {
            String ligne;
            while ((ligne = reader.readLine()) != null) {
                System.out.println(ligne);
            }
        } catch (IOException e) {
            System.out.println("Une erreur est survenue lors de la lecture du fichier scoreboard.txt : " + e.getMessage());
        }
    }

    /**
     * Lit, trie et affiche les scores du scoreboard en ordre croissant.
     */
    public static void lireEtTrierScores() {
        List<String> scores = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fichierScoreboard))) {
            String ligne;
            while ((ligne = reader.readLine()) != null) {
                scores.add(ligne);
            }
        } catch (IOException e) {
            System.out.println("Une erreur est survenue lors de la lecture du fichier scoreboard.txt : " + e.getMessage());
            return;
        }
        Collections.sort(scores, new Comparator<String>() {
            public int compare(String score1, String score2) {
                int score1Value = Integer.parseInt(score1.split(":")[1].trim());
                int score2Value = Integer.parseInt(score2.split(":")[1].trim());
                return Integer.compare(score1Value, score2Value);
            }
        });

        // Afficher les scores triés avec "1er", "2ème", ...
        System.out.println("Top Classement :");
        int classement = 1;
        for (String score : scores) {
            System.out.println(classement + "* " + score);
            classement++;
        }
    }
}
