package fr.afpalangueux.lejusteprix;

import java.util.Scanner;

public class Demande {

    public static String prenom() {
        /**
         * Permet à l'utilisateur de saisir son prénom et affiche un message de bienvenue.
         *
         * @return Le prénom saisi par l'utilisateur.
         */
        Scanner inputName = new Scanner(System.in);
        Print.print("Entrez votre Nom : ");
        String prenom = inputName.nextLine();
        Print.print("Bienvenue " + prenom + " dans Le Juste Prix\n");
        return prenom; // Retournez le prénom saisi
    }


    public static void pressAnyKeyToContinue() {
        /**
         * Attend que l'utilisateur appuie sur la touche Entrée pour continuer.
         */
        Print.print("Appuyez sur Entrer pour continuer...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine(); // Attend que l'utilisateur appuie sur Entrée
    }

}