package fr.afpalangueux.lejusteprix;

public class Print {

    public static void print(String texte) {
        /**
         * Cette focntion a pour but un gain de temps et de place en remplacant "System.out.println(texte)" par "print(texte)"
         *
         * @param texte Le texte à afficher.
         */
        System.out.println(texte);
    }
}
