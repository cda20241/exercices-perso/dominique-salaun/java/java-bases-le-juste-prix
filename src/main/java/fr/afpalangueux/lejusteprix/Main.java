package fr.afpalangueux.lejusteprix;

public class Main {

    /**
     * Le point d'entrée du programme. Appelle la méthode de vérification du tableau de bord
     * et lance le menu du jeu Le Juste Prix.
     *
     * @param args Les arguments de ligne de commande (non utilisés dans ce programme).
     */
    public static void main(String[] args) {
        Scoreboard.verif();
        LeJustePrix.startMenu();
    }
}